<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User as User;

class UserController extends Controller
{   
    public function createUser(Request $request)
    {
        //$typeUser = (int) $request->input('typeUser');
        $email = $request->input('email');
        $password = Hash::make($request->input('password'));
        $user = new User;
        $user->login = $email;
        $user->clave = $password;
        $user->id_tipo_usuario = User::CLIENT_USER;
        $user->cedula = $request->input('cedula');
        $user->nombres = $request->input('full-name');
        $user->correo = $email;
        $user->es_rv = 0;
        $user->rv_asociado = 'sin definir';
        $user->pregunta_secreta = $request->input('pregunta-secreta');
        $user->respuesta_secreta = $request->input('respuesta-secreta');
        $user->fecha_creacion = date('Y-m-d H:i:s');
        $user->activo = User::STATUS_ACTIVE;

        if ($this->existsInDb($user, 'correo', $email))         
            return response()->json(['success' => false, 'description' => 'El usuario ya se encuentra registrado']);
         
        $user->save();
        return response()->json(['success' => true, 'description' => 'Usuario creado correctamente']);
    }
    
    public function checkLogin(Request $request)
    {        
        $user = User::where('login', $request->input('email'))->first();        
        if (!$user)         
            return response()->json(['success' => false, 'description' => 'Usuario y clave inválidos']);
        
        elseif (Hash::check($request->input('password'), $user->clave) || $request->input('password') === $user->clave)
        {
            session(['logged' => true, 'username' => $user->login]);
            return response()->json(['success' => true, 'description' => '']);
        }
        return response()->json(['success' => false, 'description' => 'Usuario y clave inválidos']);
    }
}
