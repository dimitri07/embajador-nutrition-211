<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Product as Product;
use App\ProductByCategory as ProductByCategory;
use App\Category as Category;

class CartController extends Controller
{
    public function __construct()
    {
        // ...
    }

    public function getProductsInCart()
    {        
        $cart = session('cart');
        return response()->json($cart); 
    }

    public function addToCart(Request $request)
    {
        $idProduct = (int) $request->input('idProduct');
        $quantity = (int) $request->input('quantity');
        $product = Product::find($idProduct); 
        $productCategory = DB::table('insumos_categoria')
            ->join('categorias', 'insumos_categoria.id_categoria', '=', 'categorias.id')
            ->select('insumos_categoria.id_insumo', 'categorias.id', 'categorias.descripcion')
            ->where('insumos_categoria.id_insumo', '=', $idProduct)
            ->first();        
        $cart = session('cart');
        if (!$cart) 
        {
            $cart = [
                $idProduct => [
                    'name'                  => $product->descripcion,
                    'nameWithFlavor'        => $product->descripcion . ' (' . $product->sabor . ') ',
                    'description'           => $product->caracteristicas,
                    'price'                 => $product->precio_3,
                    'image'                 => $product->foto_1, 
                    'quantity'              => $quantity,
                    'codigo'                => $product->codigo,
                    'idCategory'            => $productCategory->id,
                    'descriptionCategory'   => $productCategory->descripcion,                 
                    'brand'                 => $product->marca,
                    'taste'                 => $product->sabor,
                    'id_father'             => $product->id_padre,
                    'content'               => $product->presentacion,
                ] 
            ];
        }
        elseif (isset($cart[$idProduct]))
        {
            $cart[$idProduct]['quantity'] += $quantity;
        }
        else
        {
            $cart[$idProduct] = [
                'name'                  => $product->descripcion,
                'nameWithFlavor'        => $product->descripcion . ' (' . $product->sabor . ') ',
                'description'           => $product->caracteristicas,
                'price'                 => $product->precio_3,
                'image'                 => $product->foto_1, 
                'quantity'              => $quantity,
                'codigo'                => $product->codigo,
                'idCategory'            => $productCategory->id,
                'descriptionCategory'   => $productCategory->descripcion,                 
                'brand'                 => $product->marca,
                'taste'                 => $product->sabor,
                'id_father'             => $product->id_padre,
                'content'               => $product->presentacion,
            ]; 
        }
        session(['cart' => $cart]);
        return response()->json($cart);
    }

    public function removeProductFromCart(Request $request)
    {
        $idProduct = (int) $request->input('idProduct');
        $cart = session('cart');
        unset($cart[$idProduct]);
        session(['cart' => $cart]);
        return response()->json($cart);   
    }

    public function deleteCartContent(Request $request)
    {
        if (session('cart')) 
            $request->session()->forget('cart');
    }

    public function showCartView()
    {
        return view('cart');
    }
}
