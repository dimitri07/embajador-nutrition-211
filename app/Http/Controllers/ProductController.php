<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product as Product;
use App\Inventory as Inventory;
use PhpParser\Node\Stmt\TryCatch;

class ProductController extends Controller
{
    public function showProduct()
    {
        return view('simpleproduct');  
    }

    public function showSimpleProduct($id)
    {
        try {
            Product::findOrFail($id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $th) {
            return view('notfound404');
        }
        
        return view('simpleproduct', ['idProduct' => (int) $id]);
    }

    public function getInfoProduct(Request $request)
    {
        $idProduct = (int) $request->input('idProduct');
        $product = Product::find($idProduct);
        //var_dump($idProduct);
        //var_dump();
        return [
            'productData'       => $product,
            'baseUrlImage'      => asset('/media/nutrition-images/products'),
            'flavors'           => $product->getProductFlavors(),
            'unitsAvailable'    => Inventory::where('id_insumo', $idProduct)->get()->sum('cantidad')
        ];
    }
}
