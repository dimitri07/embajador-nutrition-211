<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order as Order;
use App\OrderItem as OrderItem; 
use App\OrderStatus as OrderStatus;
use App\Category as Category;

class OrderController extends Controller
{
    public function goToCheckout()
    {
        return view('checkout');
    }

    public function proccessCheckout(Request $request)
    {
        $order = $this->createOrder(
            $request->input('contact-person'),
            $request->input('identification'),
            $request->input('contact-phone'),
            $request->input('contact-email'),
            $request->input('mailing-address'),
            $request->input('courier'),
            $request->input('observations')
        );
        $order->save();

        //save order item
        $cart = session('cart');
        foreach ($cart as $key => $product) 
        {
            $orderItem = new OrderItem;
            $orderItem->id_pedido = $order->id;
            $orderItem->id_insumo = $key;
            $orderItem->codigo_cat = $product['idCategory']; 
            $orderItem->descripcion_cat = $product['descriptionCategory'];
            $orderItem->codigo_subcat = 0;
            $orderItem->descripcion_subcat = '';
            $orderItem->codigo = $product['codigo'];
            $orderItem->descripcion = $product['name'];
            $orderItem->marca = $product['brand'];
            $orderItem->unidad = $product['content'];
            $orderItem->precio_unitario = $product['price'];
            $orderItem->descuento = 0;
            $orderItem->iva = 0;
            $orderItem->precio_total = $product['price'];
            $orderItem->cantidad_solicitada = $product['quantity'];
            
            $orderItem->save();
        }
        return response()->json(['success' => true, 'description' => 'Orden Procesada exitosamente']);
    }

    public function createOrder(        
        string $contactPerson,
        string $contactIdentification,
        string $phoneNumber,
        string $contactEmail,
        string $mailingAddress,
        string $courier,
        string $observations
    ) : Order
    {
        $order = new Order;
        $order->id_cliente = session('username');
        $order->persona_contacto = $contactPerson;
        $order->cedula_contacto = $contactIdentification;
        $order->telefono_contacto = $phoneNumber;
        $order->correo_contacto = $contactEmail;
        $order->estado_contacto = 'pendiente';
        $order->direccion_entrega = $mailingAddress;
        $order->tipo_entrega = $courier;
        $order->observaciones = $observations;
        $order->fecha = date('Y-m-d H:i:s');
        $order->tasa_dolar = 0.00;
        $order->monto_subtotal = 0.00;
        $order->monto_base_imponible = 0.00;        
        $order->monto_iva = 0.00;
        $order->monto_total = 0.00;
        $order->estatus = OrderStatus::NEW;
        
        return $order;
    }    
}
