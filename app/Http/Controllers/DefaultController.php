<?php

namespace App\Http\Controllers;

use App\Category as Category;
use App\Product as Product;
use App\ProductByCategory as ProductByCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DefaultController extends Controller
{

    public function goHome()
    {
        $productsByCategory = ProductByCategory::all();
        $productsContainer = [];
        foreach ($productsByCategory as $key => $productByCategory) 
        {
            $product = Product::find($productByCategory->id_insumo);            
            if ($product->isFather()) 
            {
                $productsContainer[] = [
                    'idCategory'    => $productByCategory->id_categoria,
                    'category'      => Category::find($productByCategory->id_categoria),
                    'mainInfo'      => Product::find($productByCategory->id_insumo),
                    'flavors'       => $product->getProductFlavors()
                ];
            }            
        }
        return view('landingpage', 
            [
                'productsFilteringByCategory' => $productsContainer,
                'categories' =>  Category::all()]
        );
    }

    public function startDashboard()
    {
        return view('dashboard', ['productList' => Product::all()]);
    }

    public function checkSession()
    {
        return response()->json(
            session('logged') ? ['logged' => true, 'username' => session('username')] : ['logged' => false]          
        );
    }    
}
