<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductByCategory extends Model
{
    protected $table = 'insumos_categoria';
}
