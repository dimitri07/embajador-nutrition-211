<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'insumos';    
    
    public function isFather() :bool
    {
        return ($this->codigo === $this->id_padre); 
    }

    public function getProductFlavors()
    {
       return self::where('id_padre', $this->codigo)->get();                
    }   


}
