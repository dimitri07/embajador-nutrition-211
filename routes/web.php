<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Example Routes
/*Route::match(['get', 'post'], '/dashboard', function(){
    return view('dashboard');
});*/
Route::get('/', 'DefaultController@goHome');

Route::get('/catalog', function(){
    return view('catalog');
});

Route::view('/examples/plugin', 'examples.plugin');
Route::view('/examples/blank', 'examples.blank');

Auth::routes();
// custom registration (with especific fields)
Route::get('/registration', function(){
    return view('customregister');
});
Route::post('/check-login', 'UserController@checkLogin');

Route::post('/create_account', 'UserController@createUser');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard', 'DefaultController@startDashboard');

Route::post('/check-session', 'DefaultController@checkSession');

Route::get('/cart', 'CartController@showCartView');

Route::get('/product-view/{id}', 'ProductController@showSimpleProduct');
Route::post('/info-product', 'ProductController@getInfoProduct');

Route::post('/products-on-cart', 'CartController@getProductsInCart');
Route::post('/add-to-cart', 'CartController@addToCart');
Route::post('/remove-from-cart', 'CartController@removeProductFromCart');
Route::post('/delete-cart-content', 'CartController@deleteCartContent');
Route::get('/checkout', 'OrderController@goToCheckout');
Route::post('/send-form-checkout', 'OrderController@proccessCheckout');

