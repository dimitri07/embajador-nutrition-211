@extends('layouts.simple')

@section('content')
    <div class="container section-cart-component">        
        <shopping-cart :quantity-products="quantityProducts"></shopping-cart>
    </div>
@endsection