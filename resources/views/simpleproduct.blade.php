@extends('layouts.simple')

@section('content')
    <simple-product id-product="{{ $idProduct }}"></simple-product>
    @component('modaladdtocart')        
    @endcomponent
@endsection
