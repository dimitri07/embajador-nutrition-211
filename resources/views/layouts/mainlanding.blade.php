<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Nutrition 211 Venezuela </title>

        <meta name="description" content="Nutrition Venezuela">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <!-- Open Graph Meta -->
        <meta property="og:title" content="Nutrition 211">
        <meta property="og:site_name" content="nutrition211.com">
        <meta property="og:description" content="Productos para todo tipo de entrenamiento">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{ asset('media/favicons/favicon.png')}}">
        <link rel="icon" sizes="192x192" type="image/png" href="{{ asset('media/favicons/favicon.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('media/favicons/favicon.png') }}">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Fonts and Codebase framework -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
        <link rel="stylesheet" id="css-main" href="{{ asset('css/codebase.css') }}">

        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->
        @yield('css_after')
        <!-- Scripts -->
        <script>window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};</script>
    </head>
    <body>        
        <div id="app">
            <div id="page-container" class="sidebar-inverse side-scroll page-header-fixed page-header-glass page-header-inverse main-content-boxed enable-page-overlay">                
                @component('mainsidebar')                    
                @endcomponent
                <!-- Main Container -->
                <main id="main-container">                                    
                    <div class="bg-header-home overflow-hidden" id="id-header-home">
                        <div class="hero bg-black-op-25">
                            <div class="hero-inner">
                                <div class="content content-full text-center">
                                    @yield('main-section')                                
                                </div>
                            </div>
                        </div>
                    </div>
                    @yield('content-1')                
                    
                    @yield('content-2')                
                    
                    @yield('products-section')
            
                    @yield('banner-section')
            
                    @yield('ammassador-section')
                    
                    @yield('secondary-banner')

                    @yield('recruiting-section')
                </main>
                <!-- END Main Container -->
                @component('footer')                    
                @endcomponent
            </div>
        </div>
        <!-- END Page Container -->
        <!-- Codebase JS -->
        <script src="{{ asset('js/codebase.core.js')}}"></script>
        <script src="{{ asset('js/codebase.app.js')}}"></script>
        <!-- Laravel Scaffolding JS -->
        <script src="{{ mix('js/laravel.app.js') }}"></script>

        <script src="{{ asset('js/codebase.app.min.js')}}"></script>

        <!-- Page JS Helpers (Content Filtering helper) -->
        <script>
            jQuery(function () {
                                    Codebase.helpers('content-filter');
                                });</script>


        @yield('js_after')
    </body>
</html>
