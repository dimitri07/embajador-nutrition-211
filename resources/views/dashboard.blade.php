@extends('layouts.simple')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-4 col-xl-4 col-sm-12">
            <!-- temporary disabled 
            <div class="row">
                <h2 class="content-heading">
                    <i class="si si-briefcase mr-5"></i> Embajadores
                </h2>
            </div>
            -->
            <div class="row">
                @for($i = 1; $i <= 8; $i++)
                    <div class="col-md-12">
                        <div class="card">
                            @if ($i % 2 != 0)
                                <img class="card-img-top" src="{{ asset('/media/nutrition-images/emba-') . $i . '.jpeg'  }}" alt="Embajador Nutrition">    
                            @else
                                <img class="card-img-top" src="{{ asset('/media/nutrition-images/emba-') . $i . '.jpg'  }}" alt="Embajador Nutrition">
                            @endif                            
                            <div class="card-body bg-dark">
                                <img 
                                    src="{{ asset('/media/nutrition-images/logo nutrition final.png') }}"
                                    alt="Emabajador Nutrition"
                                    style="height: 85px; width:auto;"
                                >
                            </div>
                        </div><br>
                    </div>
                @endfor                                
            </div>
        </div>
        <div class="col-md-8 col-xl-8 col-sm-12">
            <!-- temporary disabled
            <div class="row">
                <h2 class="content-heading">                                
                    <i class="si si-briefcase mr-5"></i> Catálogo
                </h2>
            </div>
            -->
            <div class="row items-push">
                @foreach ($productList as $product)
                    <div class="col-md-6 col-xl-6 col-sm-12">
                        <div class="block block-rounded ribbon ribbon-modern ribbon-primary text-center">
                            <div class="ribbon-box"> {{ $product->price }} $ </div>
                            <div class="block-content block-content-full">
                                <a href="/product-view/{{ $product->id }}">
                                    <img src="{{ asset('/media/nutrition-images/products') .'/'. $product->product_image }}" style="height: 300px;">
                                </a>
                                <div class="text-warning">
                                    <i class="fa fa-fw fa-star"></i>
                                    <i class="fa fa-fw fa-star"></i>
                                    <i class="fa fa-fw fa-star"></i>
                                    <i class="fa fa-fw fa-star"></i>
                                    <i class="fa fa-fw fa-star"></i>
                                </div>
                            </div>
                            <div class="block-content block-content-full block-content-sm bg-body-light">
                                <div class="font-w600 mb-5"> {{ $product->name }} </div>
                                <div class="font-size-sm text-muted">{{ $product->description }}</div>
                            </div>
                            <div class="block-content block-content-full">
                                <button
                                    type="button" 
                                    class="btn btn-rounded btn-danger min-width-125 mb-10 button-add-cart"
                                    data-toggle="modal"
                                    data-target="#modal-popin"
                                    @click="getProductIdentier({{ $product->id }})"                                    
                                >
                                    <i class="fa fa-cart-plus mr-5"></i>Añadir al carrito
                                </button>                                
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <!--
            <modal-add-cart :id-product="idCurrentProduct"></modal-add-cart>         
        -->
    </div>
</div>
@endsection
