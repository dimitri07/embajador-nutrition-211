<footer id="page-footer" class="bg-white opacity-0">
    <div class="content content-full">
        <div class="text-center">
            <h3 class="h5 font-w700">Nutrition 211 Venezuela</h3>
            <div class="font-size-sm mb-30">
                Caracas, Venezuela<br>
                <!-- Dirección física: skhdkjsdhfkshdfkhsdkf<br> -->
                <abbr title="Phone">Teléfono:</abbr> (+58) 212 - 888 88 88
            </div>
        </div>
        <div class="font-size-xs clearfix border-t pt-20 pb-10">                        
            <div class="text-center">
                <a class="font-w600" href="/" target="_blank">Reservados todos los derechos</a> &copy; <span class="js-year-copy"></span>
            </div>
        </div>
    </div>
</footer>