<aside id="side-overlay">
    <!-- Side Header -->
    <div class="content-header content-header-fullrow">
        <div class="content-header-section align-parent">            
            <button 
                type="button"
                class="btn btn-circle btn-dual-secondary align-v-r"
                data-toggle="layout"
                data-action="side_overlay_close"
            >
                <i class="fa fa-times text-danger"></i>
            </button>            
            
            <div class="content-header-item">
                <a class="img-link mr-5" href="javascript:void(0)">
                    <i class="fa fa-shopping-cart"></i>                                    
                </a>
                <a class="align-middle link-effect text-primary-dark font-w600" href="javascript:void(0)">
                    Carrito - Productos agregados
                </a>
            </div>
            <!-- END User Info -->
        </div>
    </div>
    <!-- END Side Header -->
    <!-- Side Content -->
    <div class="content-side">
        <p v-if="quantityProducts == 0"> No hay productos agregados </p>        
        <p v-for="(item, index) in cartContent">
            <span v-html="item.name"></span>
            <span class="badge badge-primary badge-pill ml-1" v-html="item.quantity"></span>              
            <span class="ml-1" v-html="item.price"></span> <b>$</b> (c/u)
            <button type="button"
                class="btn btn-outline-info ml-1"
                @click="removeOfCart(index)">
                <i class="fa fa-trash"></i>
            </button>
        </p>
        <div v-if="cartTotal > 0" class="total-price">
            <hr>
            <span class="float-right badge badge-primary mr-4" v-html="cartTotal"></span>
            <span class="float-right font-weight-bold mr-4" style="padding-left: 3px; padding-right: 3px;">
                Total USD: 
            </span>                            
        </div>
        <div class="row buttons-group-cart">
            <div class="col-md-12 col-12 col-sm-12 text-center">
                <a href="/cart" class="btn btn-rounded btn-danger min-width-125 mb-10">Ver Carrito</a>
            </div>
            <div class="col-md-12 col-12 col-sm-12 text-center">
                <a href="/checkout" class="btn btn-rounded btn-danger min-width-125 mb-10">Finalizar Compra</a>
            </div>
        </div>
    </div>
    <!-- END Side Content -->
</aside>
<!-- navbar for mobile devices -->
<nav id="sidebar">
    <!-- Sidebar Content -->
    <div class="sidebar-content">
        <!-- Side Header -->
        <div class="content-header content-header-fullrow bg-black-op-10">
            <div class="content-header-section text-center align-parent">
                <!-- Close Sidebar, Visible only on mobile screens -->
                <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
                <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                    <i class="fa fa-times text-danger"></i>
                </button>
                <!-- END Close Sidebar -->
                <!-- Logo -->
                <div class="content-header-item">                                                                   
                    <img src="{{ asset('media/nutrition-images/logo nutrition final.png')}}" class="logo-home-header">                                
                </div>
                <!-- END Logo -->
            </div>
        </div>
        <!-- END Side Header -->
        <!-- Side Main Navigation -->
        <div class="content-side content-side-full">
            <!--
            Mobile navigation, desktop navigation can be found in #page-header
            If you would like to use the same navigation in both mobiles and desktops, you can use exactly the same markup inside sidebar and header navigation ul lists
            -->
            <ul class="nav-main">
                <li>
                    <a class="active" href="/">
                        <i class="si si-home"></i>Home
                    </a>
                </li>
                <li class="nav-main-heading">Heading</li>
                <li>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                        <i class="si si-puzzle"></i>Dropdowndfcds
                    </a>
                    <ul>
                        <li>
                            <a href="javascript:void(0)">Link #1</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Link #2</a>
                        </li>
                        <li>
                            <a class="nav-submenu" data-toggle="nav-submenu" href="#">Dropdown</a>
                            <ul>
                                <li>
                                    <a href="javascript:void(0)">Link #1</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Link #2</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav-main-heading">Vital</li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="si si-wrench"></i>Page
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="si si-wrench"></i>Page
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="si si-wrench"></i>Page
                    </a>
                </li>
            </ul>
        </div>
        <!-- END Side Main Navigation -->
    </div>
    <!-- Sidebar Content -->
</nav>
<!-- end navbar for mobile devices -->

<!-- Header -->
<header id="page-header">
    <!-- Header Content -->
    <div class="content-header">
        <!-- Left Section -->
        <div class="content-header-section">
            <!-- Logo -->
            <div class="content-header-item">
                <a class="font-w700 mr-5" href="/">                                
                    <img src="{{ asset('media/nutrition-images/logo nutrition final.png')}}" class="logo-home-header">
                </a>
            </div>
            <!-- END Logo -->
        </div>
        <!-- END Left Section -->
        <!-- Right Section -->
        <div class="content-header-section">
            <!-- Header Navigation -->
            <ul class="nav-main-header nav-main-header-no-icons">
                <li>
                    <a class="active" href="/">
                        <i class="si si-home"></i>Home
                    </a>
                </li>
                <li class="nav-main-heading">Categorías</li>
                <li>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                        <i class="si si-puzzle"></i>Productos
                    </a>
                    <ul>
                        <li>
                            <a href="javascript:void(0)">Categoria #1</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Categoría #2</a>
                        </li>
                        <li>
                            <a class="nav-submenu" data-toggle="nav-submenu" href="#">Dropdown</a>
                            <ul>
                                <li>
                                    <a href="javascript:void(0)">Link #1</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Link #2</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav-main-heading">Vital</li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="si si-wrench"></i>Unete al equipo
                    </a>
                </li>
                <li>
                    <a href="#ambassadors">
                        <i class="si si-wrench"></i>Representantes
                    </a>
                </li>
            </ul>
            <button 
                type="button"
                class="btn btn-rounded btn-dual-secondary"
                data-toggle="layout"
                data-action="side_overlay_toggle"
            >
                <i class="fa fa-shopping-cart"></i>
                <span class="badge badge-primary badge-pill" v-text="quantityProducts"></span>
            </button>
            <div v-if="looged" class="btn-group" role="group">
                <button                                 
                    type="button"
                    class="btn btn-rounded btn-dual-secondary"                                
                    id="page-header-user-dropdown"
                    data-toggle="dropdown"                                
                    aria-haspopup="true"
                    aria-expanded="false"                                
                >   
                    <i class="fa fa-user d-sm-none"></i>                                     
                    <span class="d-none d-sm-inline-block" v-html="username"></span>
                    <i class="fa fa-angle-down ml-5"></i>                            
                </button>
                <div class="dropdown-menu dropdown-menu-right min-width-200" aria-labelledby="page-header-user-dropdown">
                    <h5 class="h6 text-center py-10 mb-5 border-b text-uppercase">User</h5>
                    <a class="dropdown-item" href="javascript:void(0)">
                        <i class="si si-user mr-5"></i> Mi perfil
                    </a>                                   
                    <div class="dropdown-divider"></div> 
                    <a class="dropdown-item" href="javascript:void(0)" data-toggle="layout" data-action="side_overlay_toggle">
                        <i class="si si-wrench mr-5"></i> Configuración
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="javascript:void(0)">
                        <i class="si si-logout mr-5"></i> Cerrar sesión
                    </a>
                </div>
            </div>
            <div v-else class="float-right">
                <button 
                    type="button"
                    class="btn btn-rounded btn-dual-secondary"
                    data-toggle="modal" 
                    data-target="#modal-fromright"                                
                >                            
                    Ingresar
                </button>
                <modal-login></modal-login>                
            </div>
            <!-- Toggle Sidebar -->
            <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
            <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none" data-toggle="layout" data-action="sidebar_toggle">
                <i class="fa fa-navicon"></i>
            </button>
            <!-- END Toggle Sidebar -->
        </div>
        <!-- END Right Section -->
    </div>
    <!-- END Header Content -->
    <!-- Header Search -->
    <div id="page-header-search" class="overlay-header">
        <div class="content-header content-header-fullrow">
            <form>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <!-- Close Search Section -->
                        <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
                        <button type="button" class="btn btn-secondary px-15" data-toggle="layout" data-action="header_search_off">
                            <i class="fa fa-times"></i>
                        </button>
                        <!-- END Close Search Section -->
                    </div>
                    <input type="text" class="form-control" placeholder="Search or hit ESC.." id="page-header-search-input" name="page-header-search-input">
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-secondary px-15">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END Header Search -->
    <!-- Header Loader -->
    <div id="page-header-loader" class="overlay-header bg-primary">
        <div class="content-header content-header-fullrow text-center">
            <div class="content-header-item">
                <i class="fa fa-sun-o fa-spin text-white"></i>
            </div>
        </div>
    </div>
    <!-- END Header Loader -->
</header>
<!-- END Header -->