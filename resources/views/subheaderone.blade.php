<div class="bg-image bg-image-bottom" style="background-image: url('{{ asset('/media/nutrition-images/banner-nutrition1.jpg') }}');">
    <div class="bg-primary-dark-op ">
        <div class="content content-full text-center">
            <!-- Avatar -->
            <div class="mb-15">
                <a class="img-link" href="be_pages_generic_profile.html">
                    <img class="img-avatar img-avatar96 img-avatar-thumb" src="assets/media/avatars/avatar15.jpg" alt="">
                </a>
            </div>
            <!-- END Avatar -->

            <!-- Personal -->
            <h1 class="h3 text-white font-w700 mb-10">John Smith</h1>
            <h2 class="h5 text-white-op">
                Product Manager <a class="text-primary-light" href="javascript:void(0)">@GraphicXspace</a>
            </h2>
            <!-- END Personal -->

            <!-- Actions -->
            <button type="button" class="btn btn-rounded btn-hero btn-sm btn-alt-success mb-5">
                <i class="fa fa-plus mr-5"></i> Add Friend
            </button>
            <button type="button" class="btn btn-rounded btn-hero btn-sm btn-alt-primary mb-5">
                <i class="fa fa-envelope-o mr-5"></i> Message
            </button>
            <!-- END Actions -->
        </div>
    </div>
</div>