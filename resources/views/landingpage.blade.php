@extends('layouts.mainlanding')

@section('main-section')
    <div style="padding-top: 15em;">
        <h2 class="font-w400 text-white-op mb-50 invisible" data-toggle="appear" data-class="animated fadeInDown">
            Bienvenidos
        </h2>
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xl-6">
                <a class="btn-big-button btn-our-products"
                    data-toggle="appear"
                    data-class="animated fadeInUp"
                    href="#products-home-section"                    
                >
                    Nuestros Productos
                </a>
            </div>
            <div class="col-md-6 col-sm-12 col-xl-6 margin-top-btn-create-account">
                <a class="btn-big-button btn-create-account"
                    data-toggle="appear"
                    data-class="animated fadeInUp"
                    href="/registration"                    
                >
                    Crear cuenta
                </a>    
            </div>
        </div>
            
    </div>  
    <div class="mt-4">
        <a href="#our-goals-home" class="arrow-down-home text-center mt-4">
            <i class="fa fa-arrow-circle-down"></i>
        </a>
    </div>  
@endsection

@section('content-1')
    <div class="bg-white">
        <div class="content content-full" id="our-goals-home">
            <div class="py-50 text-center">
                <h1 class="font-w700 mb-10">Nuestras metas</h1>                
            </div>
        </div>
        <div class="row">            
            <div class="col-md-4 colxs-12 col-sm-12 text-center">
                <img src="{{ asset('media/nutrition-images/muscle.png') }}" class="img-goals">
                <div class="goal-title">Construcción Muscular</div>
                <div class="goal-subtitle">Suplementos que ayudan a la Construcción muscular</div>                
            </div>
            <div class="col-md-4 colxs-12 col-sm-12 text-center">
                <img src="{{ asset('media/nutrition-images/peso.png') }}" class="img-goals">
                <div class="goal-title">Control de peso</div>
                <div class="goal-subtitle">Sumplementos que ayudan a mantener el peso</div>
                
            </div>            
            <div class="col-md-4 colxs-12 col-sm-12 text-center">
                <img src="{{ asset('media/nutrition-images/man.png') }}" class="img-goals">
                <div class="goal-title">Desempeño</div>
                <div class="goal-subtitle">Suplementos para aumentar, intensificar el desempeño</div>
            </div>
        </div>
        <br><br>
    </div>    
@endsection

@section('content-2')
    <div class="bg-body-light" id="products-home-section">
        <div class="content content-full">
            <div class="py-50 text-center">
                <h1 class="font-w700 mb-10">Nuestros productos</h1>                
            </div>
        </div>
    </div>    
@endsection

@section('products-section')
    <div class="container">        
        <div class="js-filter" data-numbers="true">
            <div class="p-10 bg-white push">
                <ul class="nav nav-pills">
                    <li class="nav-item">
                        <a class="nav-link active" href="#" data-category-link="all">
                            Todos
                        </a>
                    </li>
                    @foreach ($categories as $category)
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-category-link="category-{{ $category->id }}">
                                {{ $category->descripcion }}
                            </a>
                        </li>    
                    @endforeach
                </ul>
            </div>
            <div class="row items-push">
                @foreach ($productsFilteringByCategory as $product)
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4" data-category="category-{{ $product['idCategory'] }}">
                        <div class="block block-rounded ribbon ribbon-modern ribbon-primary text-center">
                            <div class="ribbon-box"> {{ $product['mainInfo']->precio_3 }} $ </div>
                            <div class="block-content block-content-full">                             
                                <a href="#"
                                    data-toggle="modal"
                                    data-target="#modal-popin-product-description"
                                    @click="getProductIdentier({{ $product['mainInfo']->id }})">
                                    @php
                                        $product['mainInfo']->foto_1 = empty($product['mainInfo']->foto_1) ?  'no_disponible.jpg' : $product['mainInfo']->foto_1;
                                    @endphp
                                    <img src="{{ asset('/media/nutrition-images/products') .'/'. $product['mainInfo']->foto_1 }}" style="height: 300px;">
                                </a>
                                <div class="text-warning">
                                    <i class="fa fa-fw fa-star"></i>
                                    <i class="fa fa-fw fa-star"></i>
                                    <i class="fa fa-fw fa-star"></i>
                                    <i class="fa fa-fw fa-star"></i>
                                    <i class="fa fa-fw fa-star"></i>
                                </div>
                            </div>
                            <div class="block-content block-content-full block-content-sm bg-body-light">
                                <div class="font-w600 mb-5"> {{ $product['mainInfo']->descripcion }} </div>
                                <div class="font-size-md text-muted">
                                    <a href="#"
                                        data-toggle="modal"
                                        data-target="#modal-popin-product-description"
                                        @click="getProductIdentier({{ $product['mainInfo']->id }})"
                                    >
                                        Descripción del producto 
                                    </a>
                                </div>
                            </div>
                            <div class="block-content block-content-full">
                                <button
                                    type="button" 
                                    class="btn btn-rounded btn-danger min-width-125 mb-10 button-add-cart"
                                    data-toggle="modal"
                                    data-target="#modal-popin"
                                    @click="getProductIdentier({{ $product['mainInfo']->id }})"                                    
                                >
                                    <i class="fa fa-cart-plus mr-5"></i>Añadir al carrito
                                </button>                                
                            </div>
                        </div>
                    </div>    
                @endforeach

                <!--
                <div class="col-md-6 col-lg-4 col-xl-3" data-category="mountains">
                    <img class="img-fluid img-thumb" src="assets/media/photos/photo1.jpg" alt="">
                </div>
                <div class="col-md-6 col-lg-4 col-xl-3" data-category="mountains">
                    <img class="img-fluid img-thumb" src="assets/media/photos/photo2.jpg" alt="">
                </div>
                <div class="col-md-6 col-lg-4 col-xl-3" data-category="water">
                    <img class="img-fluid img-thumb" src="assets/media/photos/photo3.jpg" alt="">
                </div>
                <div class="col-md-6 col-lg-4 col-xl-3" data-category="nature">
                    <img class="img-fluid img-thumb" src="assets/media/photos/photo5.jpg" alt="">
                </div>
                <div class="col-md-6 col-lg-4 col-xl-3" data-category="mountains">
                    <img class="img-fluid img-thumb" src="assets/media/photos/photo6.jpg" alt="">
                </div>
                <div class="col-md-6 col-lg-4 col-xl-3" data-category="nature">
                    <img class="img-fluid img-thumb" src="assets/media/photos/photo8.jpg" alt="">
                </div>
                <div class="col-md-6 col-lg-4 col-xl-3" data-category="water">
                    <img class="img-fluid img-thumb" src="assets/media/photos/photo9.jpg" alt="">
                </div>
                <div class="col-md-6 col-lg-4 col-xl-3" data-category="nature">
                    <img class="img-fluid img-thumb" src="assets/media/photos/photo11.jpg" alt="">
                </div>
                <div class="col-md-6 col-lg-4 col-xl-3" data-category="nature">
                    <img class="img-fluid img-thumb" src="assets/media/photos/photo13.jpg" alt="">
                </div>
                <div class="col-md-6 col-lg-4 col-xl-3" data-category="mountains">
                    <img class="img-fluid img-thumb" src="assets/media/photos/photo21.jpg" alt="">
                </div>
                <div class="col-md-6 col-lg-4 col-xl-3" data-category="mountains">
                    <img class="img-fluid img-thumb" src="assets/media/photos/photo22.jpg" alt="">
                </div>
                <div class="col-md-6 col-lg-4 col-xl-3" data-category="nature">
                    <img class="img-fluid img-thumb" src="assets/media/photos/photo23.jpg" alt="">
                </div>
                -->
            </div>
        </div>
    </div>
    
    @component('modaladdtocart')        
    @endcomponent
    <modal-product-description :name="productName" :description="productDescription"></modal-product-description>
@endsection

@section('banner-section')
    <img src="{{ asset('media/nutrition-images/banner nutrition10.jpg') }}" class="banner-image">
@endsection

@section('ammassador-section')
    <div class="bg-white">
        <div class="content content-full" id="ambassadors">
            <div class="py-50 text-center">
                <h1 class="font-w700 mb-10">Nuestros representantes</h1>                
            </div>
        </div>
        <div class="container">
            <div class="row">
                @for($i = 1; $i <= 6; $i++)
                    <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 mt-2">
                        <div class="card">
                            @if ($i % 2 != 0)
                                <img class="card-img-top" src="{{ asset('/media/nutrition-images/emba-') . $i . '.jpeg'  }}" alt="Embajador Nutrition">    
                            @else
                                <img class="card-img-top" src="{{ asset('/media/nutrition-images/emba-') . $i . '.jpg'  }}" alt="Embajador Nutrition">
                            @endif                            
                            <div class="card-body bg-dark text-center">
                                <span class="text-white"> Nombre y Apellido </span>
                                
                                <img 
                                    src="{{ asset('/media/nutrition-images/logo nutrition final.png') }}"
                                    alt="Emabajador Nutrition"
                                    style="height: 65px; width:auto;"
                                >
                                
                            </div>
                        </div>
                    </div>                
                @endfor
            </div>
        </div>
    </div>    
@endsection

@section('secondary-banner')
    <div class="bg-white" style="padding-top: 3%;">
        <img src="{{ asset('media/nutrition-images/banner nutrition8.jpg') }}" class="banner-image">    
    </div>
@endsection

@section('recruiting-section')
    <div class="bg-body-light">
        <div class="content content-full text-center">
            <div class="py-50">
                <h3 class="font-w700 mb-10">Forma parte de nuestro equipo</h3>
                <h4 class="font-w400 text-muted mb-30">Conviértete en un representante de ventas</h4>                
                <a class="btn btn-hero btn-rounded btn-alt-primary" href="/registration">Registrarme</a>
            </div>
        </div>
    </div>    
@endsection