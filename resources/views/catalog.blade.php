@extends('layouts.solo')

@section('content')
    <!-- Hero -->
    <div class="bg-image" style="background-image: url('{{ asset('/media/nutrition-images/banner-nutrition1.jpg') }}'); width: 100%;">
        <div class="hero bg-white-op-90 overflow-hidden">
            <div class="hero-inner">
                <div class="content content-full text-center">
                    <div class="pt-100 pb-150">
                        <h1 class="font-w700 display-4 mt-20 invisible" data-toggle="appear" data-timeout="50">
                            <img src="{{asset('/media/nutrition-images/logo nutrition final.png') }}" style="width: 100%;">                            
                        </h1>
                        <h2 class="h3 font-w400 text-muted mb-50 invisible" data-toggle="appear" data-class="animated fadeInDown" data-timeout="300">
                            Bienvenidos! Tenemos una serie de productos especiales para cada necesidad
                        </h2>
                        <div class="invisible" data-toggle="appear" data-class="animated fadeInDown" data-timeout="300">
                            <a class="btn btn-rounded btn-danger min-width-125 mb-10" href="/dashboard">
                                <i class="fa fa-fw fa-arrow-right mr-1"></i> Ver Catálogo
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
