<!-- Modal add product to cart -->
<div class="container">
    <!-- Pop In Modal -->
    <div class="modal fade" id="modal-popin" tabindex="-1" role="dialog" aria-labelledby="modal-popin" aria-hidden="true">
        <div class="modal-dialog modal-dialog-popin" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Añadir producto al Carrito</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <form id="form-add-products">
                        <div class="block-content">
                        <div class="row">
                            <div class="col-md-6">
                                <p>Producto: <span class="font-bold" v-text="productName"></span> </p>     
                            </div>
                            <div class="col-md-6">
                                <p>Precio por unidad: <span v-text="productPrice"></span> $ </p>
                            </div>
                        </div>                            
                        <div class="form-group row">
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    <input type="number" 
                                        class="form-control"
                                        id="material-select2"
                                        name="material-select2"
                                        v-model="productQuantity">
                                    <!--
                                    <select 
                                        class="form-control"
                                        id="material-select2"
                                        name="material-select2"
                                        v-model="productQuantity"
                                    >                                        
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                    -->   
                                    <label for="material-select2">Seleccione Cantidad</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <p>Costo Total USD: </p>
                                <h4 v-text="totalPrice"></h4>   
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-12">
                            <div :class="availabilityStatusClass" style="height:100%;">
                                    <div class="float-left">Unidades disponibles: </div> 
                                    <div class="float-left" style="padding-left:2px;" v-html="unitsAvailable"></div>  
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="form-material floating">
                                    <select 
                                        class="form-control"
                                        id="material-select3"
                                        name="material-select3"
                                        v-model="flavorSelected"                                    
                                    >
                                        <option v-for="flavorProduct in flavorsOfProduct" :value="flavorProduct.id"  v-html="flavorProduct.sabor">
                                        </option>
                                    </select>
                                    <label for="material-select3">Seleccione sabor</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8 offset-md-4">
                                <button 
                                    type="button"
                                    class="btn btn-rounded btn-danger min-width-125 mb-10"
                                    @click="confirmAddProduct"
                                >
                                    <i class="fa fa-check"></i> Confirmar
                                </button>
                            </div>
                        </div>
                        <div class="row" v-if="productAdded">
                            <div class="col-md-8 offset-md-2">
                                <div class="alert alert-success">
                                    <i class="fa fa-check-circle"></i> Agregado al carrito
                                </div>
                            </div>                                                           
                        </div>
                        <div class="row" v-if="errorOnProductAdded">
                            <div class="col-md-8 offset-md-2">
                                <div class="alert alert-danger">
                                    <i class="fa fa-times-circle"></i>
                                    <span v-html="errorDescription"></span>
                                </div>
                            </div>                                                        
                        </div>                                                    
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cerrar</button>                        
                </div>
            </div>
        </div>
    </div>
    <!-- END Pop In Modal -->
</div>
<!-- end modal add product to cart -->                        