@extends('layouts.simple')

@section('content')
    <div class="custom-registration-form text-center">
        <h2 class="content-heading"> Selecciona tipo de registro </h2>
        <div class="block">
            <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#btabs-animated-slideleft-home">Cliente</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#btabs-animated-slideleft-profile">Representante de ventas</a>
                </li>
                <li class="nav-item ml-auto">
                    <a class="nav-link" href="#btabs-animated-slideleft-settings"><i class="si si-settings"></i></a>
                </li>
            </ul>
            <div class="block-content tab-content overflow-hidden">
                <div class="tab-pane fade fade-left show active" id="btabs-animated-slideleft-home" role="tabpanel">
                    <h5 class="font-w400">Quiero comprar productos para consumir</h5>
                    <div class="block block-themed">
                        <div class="block-header bg-gd-emerald">
                            <h3 class="block-title">Crear cuenta</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                                    <i class="si si-refresh"></i>
                                </button>
                                <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"></button>
                            </div>
                        </div>
                        <register-form-client></register-form-client>                        
                    </div>
                </div>
                <div class="tab-pane fade fade-left" id="btabs-animated-slideleft-profile" role="tabpanel">
                    <h5 class="font-w400">Quiero conventirme en vendedor</h5>
                    <div class="block block-themed">
                        <div class="block-header bg-gd-emerald">
                            <h3 class="block-title">Crear cuenta</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                                    <i class="si si-refresh"></i>
                                </button>
                                <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"></button>
                            </div>
                        </div>                    
                        <register-form-representative></register-form-representative>
                    </div>
                </div>
                <div class="tab-pane fade fade-left" id="btabs-animated-slideleft-settings" role="tabpanel">
                    <h4 class="font-w400">Contenido de ayuda</h4>
                    pendiente por agregar...
                </div>
            </div>
        </div>
    </div>    
@endsection

