<!-- Modal login -->
<div class="modal fade" id="modal-fromright" tabindex="-1" role="dialog" aria-labelledby="modal-fromright" aria-hidden="true">
    <div class="modal-dialog modal-dialog-fromright" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Iniciar sesión</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <div class="container">
                        <div class="row justify-content-center mt-2">
                            <div class="col-md-8">
                                <div class="card">
                                    <div class="card-body">
                                        <form method="POST" action="{{ route('login') }}">
                                            @csrf                                            
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <div class="form-material floating">
                                                        <input 
                                                            id="email"
                                                            type="email"
                                                            class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" 
                                                            name="email"
                                                            required autofocus>                                            
                                                        @if ($errors->has('email'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('email') }}</strong>
                                                            </span>
                                                        @endif
                                                        <label for="email">Correo Electrónico</label>                                                                                                                                
                                                    </div>
                                                    
                                                </div>
                                            </div>                                            
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <div class="form-material floating">
                                                        <input 
                                                            id="password"
                                                            type="password"
                                                            class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                            name="password"
                                                            required>
                                                        @if ($errors->has('password'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                        @endif
                                                        <label for="password">Contraseña</label>                                            
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-0">
                                                <div class="col-md-10 offset-md-2">
                                                    <button 
                                                        type="submit"
                                                        class="btn btn-rounded btn-danger min-width-125 mb-10">
                                                        Ingresar
                                                    </button>                                            
                                                    @if (Route::has('register'))                                                                                    
                                                        <a class="btn btn-link" href="{{ route('register') }}" style="padding-left: 0;">
                                                            Crear una cuenta
                                                        </a>                                                                                
                                                    @endif
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                            
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cerrar</button>                                    
            </div>
        </div>
    </div>
</div>
<!-- end modal login -->