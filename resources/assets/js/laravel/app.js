
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('simple-product', require('./components/SimpleProduct.vue'));
Vue.component('modal-product-description', require('./components/ModalProductDescription'));
Vue.component('register-form-representative', require('./components/RegisterFormRepresentative'));
Vue.component('register-form-client', require('./components/RegisterFormClient'));
Vue.component('modal-login', require('./components/ModalLogin.vue'));
Vue.component('shopping-cart', require('./components/ShoppingCart.vue'));
Vue.component('total-by-product', require('./components/TotalByProduct.vue'));
Vue.component('checkout-form', require('./components/CheckoutForm.vue'));
//Vue.component('session-state', require('./components/SessionState.vue'));
//Vue.component('modal-add-cart', require('./components/ModalAddCart.vue'));
// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


const app = new Vue({
    el: '#app',
    data : {
        idCurrentProduct : '',
        cartContent : {},
        looged : false,
        username : '',        
        productName : '',
        productDescription : '',
        productQuantity : 0,
        productPrice : 0,
        productAdded : false,
        errorOnProductAdded : false,
        errorDescription : '',
        flavorsOfProduct : {},
        flavorSelected : 0,
        unitsAvailable : 0,
        availabilityStatusClass : ''
    },
    mounted() {
        this.checkSession();                
        this.$root.$on('sendProductIdentifier', (id) =>  {
            this.getProductIdentier(id);
        });
        this.$root.$on('notificationUserLogged', () => {
            this.checkSession();
        })
        this.$root.$on('notificationUpdateCart', (idProduct, quantity) => {
            this.addTocart(idProduct, quantity);
            this.getProductsinCart();
        });
        this.getProductsinCart();
    },
    methods: {
        checkSession(){
            axios.post('/check-session')
            .then(response => {
                if (response.data.logged)
                {
                    this.looged = true;
                    this.username = response.data.username;                    
                } 
                else 
                    this.looged = false;                    
            })
            .catch(error => {
                console.log(error);                
            })
        },
        getProductIdentier : function(idProduct) {            
            this.idCurrentProduct = idProduct;
            this.productAdded = false;
            this.errorOnProductAdded = false;
            this.productQuantity = 0;
            this.flavorSelected = 0;
            this.disabledFormAddProduct('#form-add-products', false);
            this.getProductInfo();
        },
        getProductInfo(){
            let formData = new FormData();
            formData.append('idProduct', this.idCurrentProduct);
            axios.post('/info-product', formData)
                .then(response => {
                    //console.log(response);
                    let productData = response.data.productData;
                    this.productName = productData.descripcion;
                    this.productPrice = productData.precio_3;
                    this.productDescription = productData.caracteristicas;
                    this.flavorsOfProduct = response.data.flavors;
                    this.unitsAvailable = response.data.unitsAvailable;
                    this.availabilityStatusClass = this.unitsAvailable == 0 ? 'alert alert-danger' : 
                        'alert alert-info'; 
                })
                .catch(error => {
                    console.log(error);                    
                })
        },        
        getProductsinCart(){
            axios.post('/products-on-cart')
                .then(response => {
                    //console.log(response);
                    this.cartContent = response.data;                    
                })
                .catch(error => {
                    console.log(error);                    
                })    
        }, 
        addTocart(idProduct, quantity){
            let formData = new FormData();
            formData.append('idProduct', parseInt(idProduct));
            formData.append('quantity', parseInt(quantity));
            axios.post('/add-to-cart', formData)
                .then(response => {
                    //console.log(response);                    
                })
                .catch(error => {
                    console.log(error);                    
                })
        },        
        confirmAddProduct(){
            this.validateInputAddProductToCart();
            if (this.errorOnProductAdded) 
                return;

            let formData = new FormData();
            this.flavorSelected = parseInt(this.flavorSelected);
            this.idCurrentProduct = (this.flavorSelected > 0) ? this.flavorSelected : parseInt(this.idCurrentProduct);
            formData.append('idProduct', this.idCurrentProduct);
            formData.append('quantity', parseInt(this.productQuantity));
            axios.post('/add-to-cart', formData)
                .then(response => {                    
                    // assing object with cart content
                    this.cartContent = response.data;
                    this.productAdded = true;
                    this.disabledFormAddProduct('#form-add-products', true);
                })
                .catch(error => {
                    console.log(error);
                    this.errorOnProductAdded = true;                
                })
        },
        removeOfCart(id){            
            let formData = new FormData();
            formData.append('idProduct', id);
            axios.post('/remove-from-cart', formData)
                .then(response =>{
                    //console.log(response);
                    this.cartContent = response.data;                    
                })
                .catch(error => {
                    console.log(error);                    
                })
        },
        disabledFormAddProduct(selector, disabled){
            $(selector + " :input").attr("disabled", disabled);            
        },
        validateInputAddProductToCart(){
            if (this.idCurrentProduct <= 0) {
                this.errorOnProductAdded = true;
                this.errorDescription = 'Error en la selección del producto';                 
            }
            else if(this.productQuantity <= 0) {
                this.errorOnProductAdded = true;
                this.errorDescription = 'Seleccione una cantidad validad de unidades';
            }
            else if (this.productQuantity > this.unitsAvailable) {
                this.errorOnProductAdded = true;
                this.errorDescription = 'Seleccione una cantidad menor o igual al la cantidad de unidades disponibles';
            }
            else if(isNaN(this.productQuantity)) {
                this.errorOnProductAdded = true;
                this.errorDescription = 'Seleccione una cantidad válidad de productos por favor';
            }
            else
                this.errorOnProductAdded = false;            
        }        
    },
    computed: {
        totalPrice : function() {
            return  Number.parseFloat(this.productPrice * this.productQuantity).toFixed(2);
        },
        quantityProducts : function() {
            let acumQuantity = 0;
            for (const key in this.cartContent) {
                if (this.cartContent.hasOwnProperty(key)) {
                    acumQuantity += parseInt(this.cartContent[key].quantity);
                }
            }
            return acumQuantity;
        },
        cartTotal : function(){
            let acumPrice = 0;
            for (const key in this.cartContent) {
                if (this.cartContent.hasOwnProperty(key)) {                    
                    acumPrice = acumPrice + (parseFloat(this.cartContent[key].price) * parseFloat(this.cartContent[key].quantity));
                }
            }
            return Number.parseFloat(acumPrice).toFixed(2);
        }
    },
    watch: {
        //...
    }
});


