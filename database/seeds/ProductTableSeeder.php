<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //factory(App\Product::class, 15)->create();
        // Real products
        DB::table('products')->insert([
            'name'              => 'Lean Gainz ' ,
            'description'       => 'Ganador de suero y carne de res que equilibra la cantidad de proteínas, grasas y carbohidratos, que son 
                escenciales para una nutrición adecuada y el desarrollo muscular...',
            'price'             => 69.99,
            'product_image'     => 'lean gainz.png',
            'available'         => 1,
            'created_at'        => date("Y-m-d H:i:s")
        ]);
        DB::table('products')->insert([
            'name'              => 'BCAA PLUS',
            'description'       => 'Apoya a la recuperación del ejercicio y a la sintesis de proteína - Relación 4:1:1 para más LEUCINA por 
                porción - Libera Leucina lentamente para prolongar la actividad Anabólica',
            'price'             => 41.99,
            'product_image'     => 'bcaa plus.png',
            'available'         => 1,
            'created_at'        => date("Y-m-d H:i:s")
        ]);
        DB::table('products')->insert([
            'name'              => 'B-NOX ANDRORUSH',
            'description'       => '',
            'price'             => 54.99,
            'product_image'     => 'b-nox androrush.png',
            'available'         => 1,
            'created_at'        => date("Y-m-d H:i:s")
        ]);
        DB::table('products')->insert([
            'name'              => 'B-NOX RIPPED',
            'description'       => '',
            'price'             => 54.9,
            'product_image'     => 'b-nox ripped.png',
            'available'         => 1,
            'created_at'        => date("Y-m-d H:i:s")
        ]);
        DB::table('products')->insert([
            'name'              => 'TEST HP',
            'description'       => 'Es un poderoso potenciador de la producción natural de la Testosterona',
            'price'             => 69.99,
            'product_image'     => 'test hp.png',
            'available'         => 1,
            'created_at'        => date("Y-m-d H:i:s")
        ]);
        DB::table('products')->insert([
            'name'              => 'ANDRORUSH',
            'description'       => 'Se ha convertido en sinónimo del soporte <<durante el entrenamiento>> para la respuesta natural de testosterona 
                al ejericio proporcionado por B-NOX',
            'price'             => 69.99,
            'product_image'     => 'androrush.png',
            'available'         => 1,
            'created_at'        => date("Y-m-d H:i:s")
        ]);
    }
}
