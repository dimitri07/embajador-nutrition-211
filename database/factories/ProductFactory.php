<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {    
    return [
        'name'          => $faker->name,
        'description'   => $faker->text,
        'price'         => rand(45, 50),
        'product_image' => 'product' . (string) rand(1, 6) . '.' . 'png',
        'available'     => true
    ];
});
